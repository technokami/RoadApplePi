#!/bin/bash
softwareVersion=$(git describe --long)

echo -e "\e[1;4;246mRoadApplePi Setup $softwareVersion\e[0m
Welcome to RoadApplePi setup. RoadApplePi is \"Black Box\" software that
can be retrofitted into any car with an OBD port. This software is meant
to be installed on a Raspberry Pi running unmodified Raspbian Stretch,
but it may work on other OSs or along side other programs and modifications.
Use with anything other then out-of-the-box Vanilla Raspbain Stretch is not
supported.

This script will download, compile, and install the necessary dependencies
before finishing installing RoadApplePi itself. Depending on your model of
Raspberry Pi, this may take several hours.
"
#!/bin/bash
if [ $# -ge 1 ]
then
    $answer = $1
else
    #Prompt user if they want to continue
	read -p "Would you like to continue? (y/N) " answer
fi

if [ "${answer^}" == "N" ] || [ "$answer" == "" ]
then
	echo "Setup aborted"
	exit
fi

#################
# Update System #
#################
echo -e "\e[1;4;93mStep 1. Updating system...\e[0m"
sudo apt-get update -qq
sudo apt-get upgrade -qq -y

###########################################
# Install pre-built dependencies from Apt #
###########################################
echo -e "\e[1;4;93mStep 2. Install pre-built dependencies from Apt...\e[0m"
sudo apt-get install -y dnsmasq hostapd libbluetooth-dev apache2 php7.3 php7.3-mysql php7.3-bcmath mariadb-server libmariadbclient-dev libmariadbclient-dev-compat uvcdynctrl
sudo systemctl disable hostapd dnsmasq

################
# Build FFMpeg #
################
ffmpegLocation=$(which ffmpeg)
if [ $? != 0 ]
then
	echo -e "\e[1;4;93mStep 3. Building ffmpeg...\e[0m"
	wget https://www.ffmpeg.org/releases/ffmpeg-4.3.1.tar.gz
	tar -xvf ffmpeg-4.3.1.tar.gz
	cd ffmpeg-4.3.1
	./configure --enable-gpl --enable-nonfree --enable-mmal --enable-omx --enable-omx-rpi
	make -j$(nproc)
	sudo make install
	cd ..
else
	echo "\e[1;4;93mStep 3. FFMpeg is already installed. Skipping...\e[0m"
fi

#######################
# Install RoadApplePi #
#######################
echo -e "\e[1;4;93mStep 4. Building and installing RoadApplePi\e[0m"
make
sudo make install

sudo cp -r html /var/www/
sudo rm /var/www/html/index.html
sudo chown -R www-data:www-data /var/www/html
sudo chmod -R 0755 /var/www/html
sudo cp raprec.service /lib/systemd/system
sudo chown root:root /lib/systemd/system/raprec.service
sudo chmod 0755 /lib/systemd/system/raprec.service
sudo systemctl daemon-reload
sudo systemctl enable raprec
sudo cp hostapd-rap.conf /etc/hostapd
sudo cp dnsmasq.conf /etc

#If videos folder does not exist, create it
if [ ! -d "/var/www/html/videos" ]; then
    sudo mkdir /var/www/html/videos
fi

sudo chown -R www-data:www-data /var/www/html

installDate=lastUpdated=$(date)
cp roadapplepi.sql roadapplepi-configd.sql
echo "INSERT INTO env (name, value) VALUES (\"rapVersion\", \"$softwareVersion\"), (\"installDate\", \"$installDate\"), (\"lastUpdated\", \"$lastUpdated\");" >> roadapplepi-configd.sql
sudo mysql < roadapplepi-configd.sql

echo "Installation Complete! Restarting in 10 seconds..."
sleep 10
sudo shutdown -r now